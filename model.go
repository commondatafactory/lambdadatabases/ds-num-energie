/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

        This codebase solves:

	The need to have an blazing fast API on
	a tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

/*
 * Options lists keys in registerReduce.
 * used for suggestions of debugging.
 */
func (r registerReduce) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

/*
 * Options lists keys in registerGroupByFunc.
 *
 */

func (r registerGroupByFunc) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Numid                                         string `json:"numid"`
	Pid                                           string `json:"pid"`
	Vid                                           string `json:"vid"`
	Lid                                           string `json:"lid"`
	Sid                                           string `json:"sid"`
	Postcode                                      string `json:"postcode"`
	Straat                                        string `json:"straat"`
	Woonplaatsnaam                                string `json:"woonplaatsnaam"`
	Huisnummer                                    string `json:"huisnummer"`
	Huisletter                                    string `json:"huisletter"`
	Huisnummertoevoeging                          string `json:"huisnummertoevoeging"`
	Oppervlakte                                   string `json:"oppervlakte"`
	Woningequivalent                              string `json:"woningequivalent"`
	Gebruiksdoelen                                string `json:"gebruiksdoelen"`
	PandBouwjaar                                  string `json:"pand_bouwjaar"`
	Pc6GemiddeldeWozWaardeWoning                  string `json:"pc6_gemiddelde_woz_waarde_woning"`
	GemiddeldeGemeenteWoz                         string `json:"gemiddelde_gemeente_woz"`
	Pc6EigendomssituatiePercKoop                  string `json:"pc6_eigendomssituatie_perc_koop"`
	Pc6EigendomssituatiePercHuur                  string `json:"pc6_eigendomssituatie_perc_huur"`
	Pc6EigendomssituatieAantalWoningenCorporaties string `json:"pc6_eigendomssituatie_aantal_woningen_corporaties"`
	Netbeheerder                                  string `json:"netbeheerder"`
	Energieklasse                                 string `json:"energieklasse"`
	WoningType                                    string `json:"woning_type"`
	Sbicode                                       string `json:"sbicode"`
	GasEanCount                                   string `json:"gas_ean_count"`
	P6GrondbeslagM2                               string `json:"p6_grondbeslag_m2"`
	P6Gasm32023                                   string `json:"p6_gasm3_2023"`
	P6GasAansluitingen2023                        string `json:"p6_gas_aansluitingen_2023"`
	P6Kwh2023                                     string `json:"p6_kwh_2023"`
	P6KwhProductie2023                            string `json:"p6_kwh_productie_2023"`
	Point                                         string `json:"point"`
	Buurtcode                                     string `json:"buurtcode"`
	Buurtnaam                                     string `json:"buurtnaam"`
	Wijkcode                                      string `json:"wijkcode"`
	Wijknaam                                      string `json:"wijknaam"`
	Gemeentecode                                  string `json:"gemeentecode"`
	Gemeentenaam                                  string `json:"gemeentenaam"`
	Provincienaam                                 string `json:"provincienaam"`
	Provinciecode                                 string `json:"provinciecode"`
}

type ItemOut struct {
	Numid                                         int64  `json:"numid"`
	Pid                                           string `json:"pid"`
	Vid                                           int64  `json:"vid"`
	Lid                                           int64  `json:"lid"`
	Sid                                           int64  `json:"sid"`
	Postcode                                      string `json:"postcode"`
	Straat                                        string `json:"straat"`
	Woonplaatsnaam                                string `json:"woonplaatsnaam"`
	Huisnummer                                    int64  `json:"huisnummer"`
	Huisletter                                    string `json:"huisletter"`
	Huisnummertoevoeging                          string `json:"huisnummertoevoeging"`
	Oppervlakte                                   int64  `json:"oppervlakte"`
	Woningequivalent                              int64  `json:"woningequivalent"`
	Gebruiksdoelen                                string `json:"gebruiksdoelen"`
	PandBouwjaar                                  int64  `json:"pand_bouwjaar"`
	Pc6GemiddeldeWozWaardeWoning                  int64  `json:"pc6_gemiddelde_woz_waarde_woning"`
	GemiddeldeGemeenteWoz                         int64  `json:"gemiddelde_gemeente_woz"`
	Pc6EigendomssituatiePercKoop                  int64  `json:"pc6_eigendomssituatie_perc_koop"`
	Pc6EigendomssituatiePercHuur                  int64  `json:"pc6_eigendomssituatie_perc_huur"`
	Pc6EigendomssituatieAantalWoningenCorporaties int64  `json:"pc6_eigendomssituatie_aantal_woningen_corporaties"`
	Netbeheerder                                  string `json:"netbeheerder"`
	Energieklasse                                 string `json:"energieklasse"`
	WoningType                                    string `json:"woning_type"`
	Sbicode                                       string `json:"sbicode"`
	GasEanCount                                   int64  `json:"gas_ean_count"`
	P6GrondbeslagM2                               int64  `json:"p6_grondbeslag_m2"`
	P6Gasm32023                                   int64  `json:"p6_gasm3_2023"`
	P6GasAansluitingen2023                        int64  `json:"p6_gas_aansluitingen_2023"`
	P6Kwh2023                                     int64  `json:"p6_kwh_2023"`
	P6KwhProductie2023                            int64  `json:"p6_kwh_productie_2023"`
	Point                                         string `json:"point"`
	Buurtcode                                     string `json:"buurtcode"`
	Buurtnaam                                     string `json:"buurtnaam"`
	Wijkcode                                      string `json:"wijkcode"`
	Wijknaam                                      string `json:"wijknaam"`
	Gemeentecode                                  string `json:"gemeentecode"`
	Gemeentenaam                                  string `json:"gemeentenaam"`
	Provincienaam                                 string `json:"provincienaam"`
	Provinciecode                                 string `json:"provinciecode"`
}

type Item struct {
	Label                                         int // internal index in ITEMS
	Numid                                         int64
	Pid                                           uint32
	Vid                                           int64
	Lid                                           int64
	Sid                                           int64
	Postcode                                      uint32
	Straat                                        string
	Woonplaatsnaam                                uint32
	Huisnummer                                    int64
	Huisletter                                    uint32
	Huisnummertoevoeging                          uint32
	Oppervlakte                                   int64
	Woningequivalent                              int64
	Gebruiksdoelen                                []uint32
	PandBouwjaar                                  int64
	Pc6GemiddeldeWozWaardeWoning                  int64
	GemiddeldeGemeenteWoz                         int64
	Pc6EigendomssituatiePercKoop                  int64
	Pc6EigendomssituatiePercHuur                  int64
	Pc6EigendomssituatieAantalWoningenCorporaties int64
	Netbeheerder                                  uint32
	Energieklasse                                 uint32
	WoningType                                    uint32
	Sbicode                                       uint32
	GasEanCount                                   int64
	P6GrondbeslagM2                               int64
	P6Gasm32023                                   int64
	P6GasAansluitingen2023                        int64
	P6Kwh2023                                     int64
	P6KwhProductie2023                            int64
	Point                                         string
	Buurtcode                                     uint32
	Buurtnaam                                     uint32
	Wijkcode                                      uint32
	Wijknaam                                      uint32
	Gemeentecode                                  uint32
	Gemeentenaam                                  uint32
	Provincienaam                                 uint32
	Provinciecode                                 uint32
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	Pid.Store(i.Pid)
	Postcode.Store(i.Postcode)
	Woonplaatsnaam.Store(i.Woonplaatsnaam)
	Huisletter.Store(i.Huisletter)
	Huisnummertoevoeging.Store(i.Huisnummertoevoeging)
	Netbeheerder.Store(i.Netbeheerder)
	Energieklasse.Store(i.Energieklasse)
	WoningType.Store(i.WoningType)
	Sbicode.Store(i.Sbicode)
	Buurtcode.Store(i.Buurtcode)
	Buurtnaam.Store(i.Buurtnaam)
	Wijkcode.Store(i.Wijkcode)
	Wijknaam.Store(i.Wijknaam)
	Gemeentecode.Store(i.Gemeentecode)
	Gemeentenaam.Store(i.Gemeentenaam)
	Provincienaam.Store(i.Provincienaam)
	Provinciecode.Store(i.Provinciecode)
	gebruiksdoelen := Gebruiksdoelen.StoreArray(i.Gebruiksdoelen)

	numid, _ := strconv.ParseInt(i.Numid, 10, 64)
	vid, _ := strconv.ParseInt(i.Vid, 10, 64)
	lid, _ := strconv.ParseInt(i.Lid, 10, 64)
	sid, _ := strconv.ParseInt(i.Sid, 10, 64)
	huisnummer, _ := strconv.ParseInt(i.Huisnummer, 10, 64)
	oppervlakte, _ := strconv.ParseInt(i.Oppervlakte, 10, 64)
	woningequivalent, _ := strconv.ParseInt(i.Woningequivalent, 10, 64)
	pandbouwjaar, _ := strconv.ParseInt(i.PandBouwjaar, 10, 64)
	pc6gemiddeldewozwaardewoning, _ := strconv.ParseInt(i.Pc6GemiddeldeWozWaardeWoning, 10, 64)
	gemiddeldegemeentewoz, _ := strconv.ParseInt(i.GemiddeldeGemeenteWoz, 10, 64)
	pc6eigendomssituatieperckoop, _ := strconv.ParseInt(i.Pc6EigendomssituatiePercKoop, 10, 64)
	pc6eigendomssituatieperchuur, _ := strconv.ParseInt(i.Pc6EigendomssituatiePercHuur, 10, 64)
	pc6eigendomssituatieaantalwoningencorporaties, _ := strconv.ParseInt(i.Pc6EigendomssituatieAantalWoningenCorporaties, 10, 64)
	gaseancount, _ := strconv.ParseInt(i.GasEanCount, 10, 64)
	p6grondbeslagm2, _ := strconv.ParseInt(i.P6GrondbeslagM2, 10, 64)
	p6gasm32023, _ := strconv.ParseInt(i.P6Gasm32023, 10, 64)
	p6gasaansluitingen2023, _ := strconv.ParseInt(i.P6GasAansluitingen2023, 10, 64)
	p6kwh2023, _ := strconv.ParseInt(i.P6Kwh2023, 10, 64)
	p6kwhproductie2023, _ := strconv.ParseInt(i.P6KwhProductie2023, 10, 64)

	return Item{
		label,
		numid,
		Pid.GetIndex(i.Pid),
		vid,
		lid,
		sid,
		Postcode.GetIndex(i.Postcode),
		i.Straat,
		Woonplaatsnaam.GetIndex(i.Woonplaatsnaam),
		huisnummer,
		Huisletter.GetIndex(i.Huisletter),
		Huisnummertoevoeging.GetIndex(i.Huisnummertoevoeging),
		oppervlakte,
		woningequivalent,
		gebruiksdoelen,
		pandbouwjaar,
		pc6gemiddeldewozwaardewoning,
		gemiddeldegemeentewoz,
		pc6eigendomssituatieperckoop,
		pc6eigendomssituatieperchuur,
		pc6eigendomssituatieaantalwoningencorporaties,
		Netbeheerder.GetIndex(i.Netbeheerder),
		Energieklasse.GetIndex(i.Energieklasse),
		WoningType.GetIndex(i.WoningType),
		Sbicode.GetIndex(i.Sbicode),
		gaseancount,
		p6grondbeslagm2,
		p6gasm32023,
		p6gasaansluitingen2023,
		p6kwh2023,
		p6kwhproductie2023,
		i.Point,
		Buurtcode.GetIndex(i.Buurtcode),
		Buurtnaam.GetIndex(i.Buurtnaam),
		Wijkcode.GetIndex(i.Wijkcode),
		Wijknaam.GetIndex(i.Wijknaam),
		Gemeentecode.GetIndex(i.Gemeentecode),
		Gemeentenaam.GetIndex(i.Gemeentenaam),
		Provincienaam.GetIndex(i.Provincienaam),
		Provinciecode.GetIndex(i.Provinciecode),
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast indexed selection
func (i *Item) StoreBitArrayColumns() {
	SetBitArray("pid", i.Pid, i.Label)
	SetBitArray("postcode", i.Postcode, i.Label)
	SetBitArray("buurtcode", i.Buurtcode, i.Label)
	SetBitArray("wijkcode", i.Wijkcode, i.Label)
	SetBitArray("gemeentecode", i.Gemeentecode, i.Label)
	SetBitArray("provincienaam", i.Provincienaam, i.Label)
	SetBitArray("provinciecode", i.Provinciecode, i.Label)

}

func (i Item) Serialize() ItemOut {
	return ItemOut{
		i.Numid,
		Pid.GetValue(i.Pid),
		i.Vid,
		i.Lid,
		i.Sid,
		Postcode.GetValue(i.Postcode),
		i.Straat,
		Woonplaatsnaam.GetValue(i.Woonplaatsnaam),
		i.Huisnummer,
		Huisletter.GetValue(i.Huisletter),
		Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging),
		i.Oppervlakte,
		i.Woningequivalent,
		Gebruiksdoelen.GetArrayValue(i.Gebruiksdoelen),
		i.PandBouwjaar,
		i.Pc6GemiddeldeWozWaardeWoning,
		i.GemiddeldeGemeenteWoz,
		i.Pc6EigendomssituatiePercKoop,
		i.Pc6EigendomssituatiePercHuur,
		i.Pc6EigendomssituatieAantalWoningenCorporaties,
		Netbeheerder.GetValue(i.Netbeheerder),
		Energieklasse.GetValue(i.Energieklasse),
		WoningType.GetValue(i.WoningType),
		Sbicode.GetValue(i.Sbicode),
		i.GasEanCount,
		i.P6GrondbeslagM2,
		i.P6Gasm32023,
		i.P6GasAansluitingen2023,
		i.P6Kwh2023,
		i.P6KwhProductie2023,
		i.Point,
		Buurtcode.GetValue(i.Buurtcode),
		Buurtnaam.GetValue(i.Buurtnaam),
		Wijkcode.GetValue(i.Wijkcode),
		Wijknaam.GetValue(i.Wijknaam),
		Gemeentecode.GetValue(i.Gemeentecode),
		Gemeentenaam.GetValue(i.Gemeentenaam),
		Provincienaam.GetValue(i.Provincienaam),
		Provinciecode.GetValue(i.Provinciecode),
	}
}

func (i ItemIn) Columns() []string {
	return []string{
		"numid",
		"pid",
		"vid",
		"lid",
		"sid",
		"postcode",
		"straat",
		"woonplaatsnaam",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"oppervlakte",
		"woningequivalent",
		"gebruiksdoelen",
		"pand_bouwjaar",
		"pc6_gemiddelde_woz_waarde_woning",
		"gemiddelde_gemeente_woz",
		"pc6_eigendomssituatie_perc_koop",
		"pc6_eigendomssituatie_perc_huur",
		"pc6_eigendomssituatie_aantal_woningen_corporaties",
		"netbeheerder",
		"energieklasse",
		"woning_type",
		"sbicode",
		"gas_ean_count",
		"p6_grondbeslag_m2",
		"p6_gasm3_2023",
		"p6_gas_aansluitingen_2023",
		"p6_kwh_2023",
		"p6_kwh_productie_2023",
		"point",
		"buurtcode",
		"buurtnaam",
		"wijkcode",
		"wijknaam",
		"gemeentecode",
		"gemeentenaam",
		"provincienaam",
		"provinciecode",
	}
}

func (i ItemOut) Columns() []string {
	return []string{
		"numid",
		"pid",
		"vid",
		"lid",
		"sid",
		"postcode",
		"straat",
		"woonplaatsnaam",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"oppervlakte",
		"woningequivalent",
		"gebruiksdoelen",
		"pand_bouwjaar",
		"pc6_gemiddelde_woz_waarde_woning",
		"gemiddelde_gemeente_woz",
		"pc6_eigendomssituatie_perc_koop",
		"pc6_eigendomssituatie_perc_huur",
		"pc6_eigendomssituatie_aantal_woningen_corporaties",
		"netbeheerder",
		"energieklasse",
		"woning_type",
		"sbicode",
		"gas_ean_count",
		"p6_grondbeslag_m2",
		"p6_gasm3_2023",
		"p6_gas_aansluitingen_2023",
		"p6_kwh_2023",
		"p6_kwh_productie_2023",
		"point",
		"buurtcode",
		"buurtnaam",
		"wijkcode",
		"wijknaam",
		"gemeentecode",
		"gemeentenaam",
		"provincienaam",
		"provinciecode",
	}
}

func (i Item) Row() []string {

	return []string{
		fmt.Sprintf("%d", i.Numid),
		Pid.GetValue(i.Pid),
		fmt.Sprintf("%d", i.Vid),
		fmt.Sprintf("%d", i.Lid),
		fmt.Sprintf("%d", i.Sid),
		Postcode.GetValue(i.Postcode),
		i.Straat,
		Woonplaatsnaam.GetValue(i.Woonplaatsnaam),
		fmt.Sprintf("%d", i.Huisnummer),
		Huisletter.GetValue(i.Huisletter),
		Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging),
		fmt.Sprintf("%d", i.Oppervlakte),
		fmt.Sprintf("%d", i.Woningequivalent),
		Gebruiksdoelen.GetArrayValue(i.Gebruiksdoelen),
		fmt.Sprintf("%d", i.PandBouwjaar),
		fmt.Sprintf("%d", i.Pc6GemiddeldeWozWaardeWoning),
		fmt.Sprintf("%d", i.GemiddeldeGemeenteWoz),
		fmt.Sprintf("%d", i.Pc6EigendomssituatiePercKoop),
		fmt.Sprintf("%d", i.Pc6EigendomssituatiePercHuur),
		fmt.Sprintf("%d", i.Pc6EigendomssituatieAantalWoningenCorporaties),
		Netbeheerder.GetValue(i.Netbeheerder),
		Energieklasse.GetValue(i.Energieklasse),
		WoningType.GetValue(i.WoningType),
		Sbicode.GetValue(i.Sbicode),
		fmt.Sprintf("%d", i.GasEanCount),
		fmt.Sprintf("%d", i.P6GrondbeslagM2),
		fmt.Sprintf("%d", i.P6Gasm32023),
		fmt.Sprintf("%d", i.P6GasAansluitingen2023),
		fmt.Sprintf("%d", i.P6Kwh2023),
		fmt.Sprintf("%d", i.P6KwhProductie2023),
		i.Point,
		Buurtcode.GetValue(i.Buurtcode),
		Buurtnaam.GetValue(i.Buurtnaam),
		Wijkcode.GetValue(i.Wijkcode),
		Wijknaam.GetValue(i.Wijknaam),
		Gemeentecode.GetValue(i.Gemeentecode),
		Gemeentenaam.GetValue(i.Gemeentenaam),
		Provincienaam.GetValue(i.Provincienaam),
		Provinciecode.GetValue(i.Provinciecode),
	}
}

func (i Item) GetIndex() string {
	return GettersNumid(&i)
}

func (i Item) GetGeometry() string {
	return GettersPoint(&i)
}

// match filters Numid
func FilterNumidMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Numid == eq
}

// less then equal Numid filter
func FilterNumidlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Numid <= max
}

// greater then equal Numid filter
func FilterNumidgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Numid >= min
}

// contain filter Numid
func FilterNumidContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Numid)
	return strings.Contains(number, s)
}

// startswith filter Numid
func FilterNumidStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Numid)
	return strings.HasPrefix(number, s)
}

// getter Numid
func GettersNumid(i *Item) string {
	return fmt.Sprintf("%d", i.Numid)
}

// contain filter Pid
func FilterPidContains(i *Item, s string) bool {
	return strings.Contains(Pid.GetValue(i.Pid), s)
}

// startswith filter Pid
func FilterPidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Pid.GetValue(i.Pid), s)
}

// match filters Pid
func FilterPidMatch(i *Item, s string) bool {
	return Pid.GetValue(i.Pid) == s
}

// gte filters Pid
func FilterPidgte(i *Item, s string) bool {
	return Pid.GetValue(i.Pid) <= s
}

// lte filters Pid
func FilterPidlte(i *Item, s string) bool {
	return Pid.GetValue(i.Pid) <= s
}

// getter Pid
func GettersPid(i *Item) string {
	return Pid.GetValue(i.Pid)
} // match filters Vid
func FilterVidMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Vid == eq
}

// less then equal Vid filter
func FilterVidlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Vid <= max
}

// greater then equal Vid filter
func FilterVidgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Vid >= min
}

// contain filter Vid
func FilterVidContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Vid)
	return strings.Contains(number, s)
}

// startswith filter Vid
func FilterVidStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Vid)
	return strings.HasPrefix(number, s)
}

// getter Vid
func GettersVid(i *Item) string {
	return fmt.Sprintf("%d", i.Vid)
} // match filters Lid
func FilterLidMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Lid == eq
}

// less then equal Lid filter
func FilterLidlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Lid <= max
}

// greater then equal Lid filter
func FilterLidgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Lid >= min
}

// contain filter Lid
func FilterLidContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Lid)
	return strings.Contains(number, s)
}

// startswith filter Lid
func FilterLidStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Lid)
	return strings.HasPrefix(number, s)
}

// getter Lid
func GettersLid(i *Item) string {
	return fmt.Sprintf("%d", i.Lid)
} // match filters Sid
func FilterSidMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Sid == eq
}

// less then equal Sid filter
func FilterSidlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Sid <= max
}

// greater then equal Sid filter
func FilterSidgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Sid >= min
}

// contain filter Sid
func FilterSidContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Sid)
	return strings.Contains(number, s)
}

// startswith filter Sid
func FilterSidStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Sid)
	return strings.HasPrefix(number, s)
}

// getter Sid
func GettersSid(i *Item) string {
	return fmt.Sprintf("%d", i.Sid)
}

// contain filter Postcode
func FilterPostcodeContains(i *Item, s string) bool {
	return strings.Contains(Postcode.GetValue(i.Postcode), s)
}

// startswith filter Postcode
func FilterPostcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Postcode.GetValue(i.Postcode), s)
}

// match filters Postcode
func FilterPostcodeMatch(i *Item, s string) bool {
	return Postcode.GetValue(i.Postcode) == s
}

// gte filters Postcode
func FilterPostcodegte(i *Item, s string) bool {
	return Postcode.GetValue(i.Postcode) <= s
}

// lte filters Postcode
func FilterPostcodelte(i *Item, s string) bool {
	return Postcode.GetValue(i.Postcode) <= s
}

// getter Postcode
func GettersPostcode(i *Item) string {
	return Postcode.GetValue(i.Postcode)
}

// contain filter Straat
func FilterStraatContains(i *Item, s string) bool {
	return strings.Contains(i.Straat, s)
}

// startswith filter Straat
func FilterStraatStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Straat, s)
}

// match filters Straat
func FilterStraatMatch(i *Item, s string) bool {
	return i.Straat == s
}

// gte filters Straat
func FilterStraatgte(i *Item, s string) bool {
	return i.Straat <= s
}

// lte filters Straat
func FilterStraatlte(i *Item, s string) bool {
	return i.Straat <= s
}

// getter Straat
func GettersStraat(i *Item) string {
	return i.Straat
}

// contain filter Woonplaatsnaam
func FilterWoonplaatsnaamContains(i *Item, s string) bool {
	return strings.Contains(Woonplaatsnaam.GetValue(i.Woonplaatsnaam), s)
}

// startswith filter Woonplaatsnaam
func FilterWoonplaatsnaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Woonplaatsnaam.GetValue(i.Woonplaatsnaam), s)
}

// match filters Woonplaatsnaam
func FilterWoonplaatsnaamMatch(i *Item, s string) bool {
	return Woonplaatsnaam.GetValue(i.Woonplaatsnaam) == s
}

// gte filters Woonplaatsnaam
func FilterWoonplaatsnaamgte(i *Item, s string) bool {
	return Woonplaatsnaam.GetValue(i.Woonplaatsnaam) <= s
}

// lte filters Woonplaatsnaam
func FilterWoonplaatsnaamlte(i *Item, s string) bool {
	return Woonplaatsnaam.GetValue(i.Woonplaatsnaam) <= s
}

// getter Woonplaatsnaam
func GettersWoonplaatsnaam(i *Item) string {
	return Woonplaatsnaam.GetValue(i.Woonplaatsnaam)
} // match filters Huisnummer
func FilterHuisnummerMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Huisnummer == eq
}

// less then equal Huisnummer filter
func FilterHuisnummerlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Huisnummer <= max
}

// greater then equal Huisnummer filter
func FilterHuisnummergte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Huisnummer >= min
}

// contain filter Huisnummer
func FilterHuisnummerContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Huisnummer)
	return strings.Contains(number, s)
}

// startswith filter Huisnummer
func FilterHuisnummerStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Huisnummer)
	return strings.HasPrefix(number, s)
}

// getter Huisnummer
func GettersHuisnummer(i *Item) string {
	return fmt.Sprintf("%d", i.Huisnummer)
}

// contain filter Huisletter
func FilterHuisletterContains(i *Item, s string) bool {
	return strings.Contains(Huisletter.GetValue(i.Huisletter), s)
}

// startswith filter Huisletter
func FilterHuisletterStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Huisletter.GetValue(i.Huisletter), s)
}

// match filters Huisletter
func FilterHuisletterMatch(i *Item, s string) bool {
	return Huisletter.GetValue(i.Huisletter) == s
}

// gte filters Huisletter
func FilterHuislettergte(i *Item, s string) bool {
	return Huisletter.GetValue(i.Huisletter) <= s
}

// lte filters Huisletter
func FilterHuisletterlte(i *Item, s string) bool {
	return Huisletter.GetValue(i.Huisletter) <= s
}

// getter Huisletter
func GettersHuisletter(i *Item) string {
	return Huisletter.GetValue(i.Huisletter)
}

// contain filter Huisnummertoevoeging
func FilterHuisnummertoevoegingContains(i *Item, s string) bool {
	return strings.Contains(Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging), s)
}

// startswith filter Huisnummertoevoeging
func FilterHuisnummertoevoegingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging), s)
}

// match filters Huisnummertoevoeging
func FilterHuisnummertoevoegingMatch(i *Item, s string) bool {
	return Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging) == s
}

// gte filters Huisnummertoevoeging
func FilterHuisnummertoevoeginggte(i *Item, s string) bool {
	return Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging) <= s
}

// lte filters Huisnummertoevoeging
func FilterHuisnummertoevoeginglte(i *Item, s string) bool {
	return Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging) <= s
}

// getter Huisnummertoevoeging
func GettersHuisnummertoevoeging(i *Item) string {
	return Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging)
} // match filters Oppervlakte
func FilterOppervlakteMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Oppervlakte == eq
}

// less then equal Oppervlakte filter
func FilterOppervlaktelte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Oppervlakte <= max
}

// greater then equal Oppervlakte filter
func FilterOppervlaktegte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Oppervlakte >= min
}

// contain filter Oppervlakte
func FilterOppervlakteContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Oppervlakte)
	return strings.Contains(number, s)
}

// startswith filter Oppervlakte
func FilterOppervlakteStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Oppervlakte)
	return strings.HasPrefix(number, s)
}

// getter Oppervlakte
func GettersOppervlakte(i *Item) string {
	return fmt.Sprintf("%d", i.Oppervlakte)
} // match filters Woningequivalent
func FilterWoningequivalentMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Woningequivalent == eq
}

// less then equal Woningequivalent filter
func FilterWoningequivalentlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Woningequivalent <= max
}

// greater then equal Woningequivalent filter
func FilterWoningequivalentgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Woningequivalent >= min
}

// contain filter Woningequivalent
func FilterWoningequivalentContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Woningequivalent)
	return strings.Contains(number, s)
}

// startswith filter Woningequivalent
func FilterWoningequivalentStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Woningequivalent)
	return strings.HasPrefix(number, s)
}

// getter Woningequivalent
func GettersWoningequivalent(i *Item) string {
	return fmt.Sprintf("%d", i.Woningequivalent)
}

// contain filter Gebruiksdoelen
func FilterGebruiksdoelenContains(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		return strings.Contains(vs, s)
	}
	return false
}

// startswith filter Gebruiksdoelen
func FilterGebruiksdoelenStartsWith(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		return strings.HasPrefix(vs, s)
	}
	return false

}

// match filters Gebruiksdoelen
func FilterGebruiksdoelenMatch(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		return vs == s
	}
	return false
}

// gte filters Gebruiksdoelen
func FilterGebruiksdoelengte(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		return vs >= s
	}
	return false
}

// lte filters Gebruiksdoelen
func FilterGebruiksdoelenlte(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		return vs <= s
	}
	return false
}

// getter Gebruiksdoelen
func GettersGebruiksdoelen(i *Item) string {
	return Gebruiksdoelen.GetArrayValue(i.Gebruiksdoelen)
}

// match filters PandBouwjaar
func FilterPandBouwjaarMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.PandBouwjaar == eq
}

// less then equal PandBouwjaar filter
func FilterPandBouwjaarlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.PandBouwjaar <= max
}

// greater then equal PandBouwjaar filter
func FilterPandBouwjaargte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.PandBouwjaar >= min
}

// contain filter PandBouwjaar
func FilterPandBouwjaarContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.PandBouwjaar)
	return strings.Contains(number, s)
}

// startswith filter PandBouwjaar
func FilterPandBouwjaarStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.PandBouwjaar)
	return strings.HasPrefix(number, s)
}

// getter PandBouwjaar
func GettersPandBouwjaar(i *Item) string {
	return fmt.Sprintf("%d", i.PandBouwjaar)
} // match filters Pc6GemiddeldeWozWaardeWoning
func FilterPc6GemiddeldeWozWaardeWoningMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Pc6GemiddeldeWozWaardeWoning == eq
}

// less then equal Pc6GemiddeldeWozWaardeWoning filter
func FilterPc6GemiddeldeWozWaardeWoninglte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6GemiddeldeWozWaardeWoning <= max
}

// greater then equal Pc6GemiddeldeWozWaardeWoning filter
func FilterPc6GemiddeldeWozWaardeWoninggte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6GemiddeldeWozWaardeWoning >= min
}

// contain filter Pc6GemiddeldeWozWaardeWoning
func FilterPc6GemiddeldeWozWaardeWoningContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6GemiddeldeWozWaardeWoning)
	return strings.Contains(number, s)
}

// startswith filter Pc6GemiddeldeWozWaardeWoning
func FilterPc6GemiddeldeWozWaardeWoningStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6GemiddeldeWozWaardeWoning)
	return strings.HasPrefix(number, s)
}

// getter Pc6GemiddeldeWozWaardeWoning
func GettersPc6GemiddeldeWozWaardeWoning(i *Item) string {
	return fmt.Sprintf("%d", i.Pc6GemiddeldeWozWaardeWoning)
} // match filters GemiddeldeGemeenteWoz
func FilterGemiddeldeGemeenteWozMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.GemiddeldeGemeenteWoz == eq
}

// less then equal GemiddeldeGemeenteWoz filter
func FilterGemiddeldeGemeenteWozlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.GemiddeldeGemeenteWoz <= max
}

// greater then equal GemiddeldeGemeenteWoz filter
func FilterGemiddeldeGemeenteWozgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.GemiddeldeGemeenteWoz >= min
}

// contain filter GemiddeldeGemeenteWoz
func FilterGemiddeldeGemeenteWozContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.GemiddeldeGemeenteWoz)
	return strings.Contains(number, s)
}

// startswith filter GemiddeldeGemeenteWoz
func FilterGemiddeldeGemeenteWozStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.GemiddeldeGemeenteWoz)
	return strings.HasPrefix(number, s)
}

// getter GemiddeldeGemeenteWoz
func GettersGemiddeldeGemeenteWoz(i *Item) string {
	return fmt.Sprintf("%d", i.GemiddeldeGemeenteWoz)
} // match filters Pc6EigendomssituatiePercKoop
func FilterPc6EigendomssituatiePercKoopMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Pc6EigendomssituatiePercKoop == eq
}

// less then equal Pc6EigendomssituatiePercKoop filter
func FilterPc6EigendomssituatiePercKooplte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6EigendomssituatiePercKoop <= max
}

// greater then equal Pc6EigendomssituatiePercKoop filter
func FilterPc6EigendomssituatiePercKoopgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6EigendomssituatiePercKoop >= min
}

// contain filter Pc6EigendomssituatiePercKoop
func FilterPc6EigendomssituatiePercKoopContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6EigendomssituatiePercKoop)
	return strings.Contains(number, s)
}

// startswith filter Pc6EigendomssituatiePercKoop
func FilterPc6EigendomssituatiePercKoopStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6EigendomssituatiePercKoop)
	return strings.HasPrefix(number, s)
}

// getter Pc6EigendomssituatiePercKoop
func GettersPc6EigendomssituatiePercKoop(i *Item) string {
	return fmt.Sprintf("%d", i.Pc6EigendomssituatiePercKoop)
} // match filters Pc6EigendomssituatiePercHuur
func FilterPc6EigendomssituatiePercHuurMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Pc6EigendomssituatiePercHuur == eq
}

// less then equal Pc6EigendomssituatiePercHuur filter
func FilterPc6EigendomssituatiePercHuurlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6EigendomssituatiePercHuur <= max
}

// greater then equal Pc6EigendomssituatiePercHuur filter
func FilterPc6EigendomssituatiePercHuurgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6EigendomssituatiePercHuur >= min
}

// contain filter Pc6EigendomssituatiePercHuur
func FilterPc6EigendomssituatiePercHuurContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6EigendomssituatiePercHuur)
	return strings.Contains(number, s)
}

// startswith filter Pc6EigendomssituatiePercHuur
func FilterPc6EigendomssituatiePercHuurStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6EigendomssituatiePercHuur)
	return strings.HasPrefix(number, s)
}

// getter Pc6EigendomssituatiePercHuur
func GettersPc6EigendomssituatiePercHuur(i *Item) string {
	return fmt.Sprintf("%d", i.Pc6EigendomssituatiePercHuur)
} // match filters Pc6EigendomssituatieAantalWoningenCorporaties
func FilterPc6EigendomssituatieAantalWoningenCorporatiesMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.Pc6EigendomssituatieAantalWoningenCorporaties == eq
}

// less then equal Pc6EigendomssituatieAantalWoningenCorporaties filter
func FilterPc6EigendomssituatieAantalWoningenCorporatieslte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6EigendomssituatieAantalWoningenCorporaties <= max
}

// greater then equal Pc6EigendomssituatieAantalWoningenCorporaties filter
func FilterPc6EigendomssituatieAantalWoningenCorporatiesgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.Pc6EigendomssituatieAantalWoningenCorporaties >= min
}

// contain filter Pc6EigendomssituatieAantalWoningenCorporaties
func FilterPc6EigendomssituatieAantalWoningenCorporatiesContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6EigendomssituatieAantalWoningenCorporaties)
	return strings.Contains(number, s)
}

// startswith filter Pc6EigendomssituatieAantalWoningenCorporaties
func FilterPc6EigendomssituatieAantalWoningenCorporatiesStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.Pc6EigendomssituatieAantalWoningenCorporaties)
	return strings.HasPrefix(number, s)
}

// getter Pc6EigendomssituatieAantalWoningenCorporaties
func GettersPc6EigendomssituatieAantalWoningenCorporaties(i *Item) string {
	return fmt.Sprintf("%d", i.Pc6EigendomssituatieAantalWoningenCorporaties)
}

// contain filter Netbeheerder
func FilterNetbeheerderContains(i *Item, s string) bool {
	return strings.Contains(Netbeheerder.GetValue(i.Netbeheerder), s)
}

// startswith filter Netbeheerder
func FilterNetbeheerderStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Netbeheerder.GetValue(i.Netbeheerder), s)
}

// match filters Netbeheerder
func FilterNetbeheerderMatch(i *Item, s string) bool {
	return Netbeheerder.GetValue(i.Netbeheerder) == s
}

// gte filters Netbeheerder
func FilterNetbeheerdergte(i *Item, s string) bool {
	return Netbeheerder.GetValue(i.Netbeheerder) <= s
}

// lte filters Netbeheerder
func FilterNetbeheerderlte(i *Item, s string) bool {
	return Netbeheerder.GetValue(i.Netbeheerder) <= s
}

// getter Netbeheerder
func GettersNetbeheerder(i *Item) string {
	return Netbeheerder.GetValue(i.Netbeheerder)
}

// contain filter Energieklasse
func FilterEnergieklasseContains(i *Item, s string) bool {
	return strings.Contains(Energieklasse.GetValue(i.Energieklasse), s)
}

// startswith filter Energieklasse
func FilterEnergieklasseStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Energieklasse.GetValue(i.Energieklasse), s)
}

// match filters Energieklasse
func FilterEnergieklasseMatch(i *Item, s string) bool {
	return Energieklasse.GetValue(i.Energieklasse) == s
}

// gte filters Energieklasse
func FilterEnergieklassegte(i *Item, s string) bool {
	return Energieklasse.GetValue(i.Energieklasse) <= s
}

// lte filters Energieklasse
func FilterEnergieklasselte(i *Item, s string) bool {
	return Energieklasse.GetValue(i.Energieklasse) <= s
}

// getter Energieklasse
func GettersEnergieklasse(i *Item) string {
	return Energieklasse.GetValue(i.Energieklasse)
}

// contain filter WoningType
func FilterWoningTypeContains(i *Item, s string) bool {
	return strings.Contains(WoningType.GetValue(i.WoningType), s)
}

// startswith filter WoningType
func FilterWoningTypeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(WoningType.GetValue(i.WoningType), s)
}

// match filters WoningType
func FilterWoningTypeMatch(i *Item, s string) bool {
	return WoningType.GetValue(i.WoningType) == s
}

// gte filters WoningType
func FilterWoningTypegte(i *Item, s string) bool {
	return WoningType.GetValue(i.WoningType) <= s
}

// lte filters WoningType
func FilterWoningTypelte(i *Item, s string) bool {
	return WoningType.GetValue(i.WoningType) <= s
}

// getter WoningType
func GettersWoningType(i *Item) string {
	return WoningType.GetValue(i.WoningType)
}

// contain filter Sbicode
func FilterSbicodeContains(i *Item, s string) bool {
	return strings.Contains(Sbicode.GetValue(i.Sbicode), s)
}

// startswith filter Sbicode
func FilterSbicodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Sbicode.GetValue(i.Sbicode), s)
}

// match filters Sbicode
func FilterSbicodeMatch(i *Item, s string) bool {
	return Sbicode.GetValue(i.Sbicode) == s
}

// gte filters Sbicode
func FilterSbicodegte(i *Item, s string) bool {
	return Sbicode.GetValue(i.Sbicode) <= s
}

// lte filters Sbicode
func FilterSbicodelte(i *Item, s string) bool {
	return Sbicode.GetValue(i.Sbicode) <= s
}

// getter Sbicode
func GettersSbicode(i *Item) string {
	return Sbicode.GetValue(i.Sbicode)
} // match filters GasEanCount
func FilterGasEanCountMatch(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.GasEanCount == eq
}

// less then equal GasEanCount filter
func FilterGasEanCountlte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.GasEanCount <= max
}

// greater then equal GasEanCount filter
func FilterGasEanCountgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.GasEanCount >= min
}

// contain filter GasEanCount
func FilterGasEanCountContains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.GasEanCount)
	return strings.Contains(number, s)
}

// startswith filter GasEanCount
func FilterGasEanCountStartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.GasEanCount)
	return strings.HasPrefix(number, s)
}

// getter GasEanCount
func GettersGasEanCount(i *Item) string {
	return fmt.Sprintf("%d", i.GasEanCount)
} // match filters P6GrondbeslagM2
func FilterP6GrondbeslagM2Match(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.P6GrondbeslagM2 == eq
}

// less then equal P6GrondbeslagM2 filter
func FilterP6GrondbeslagM2lte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6GrondbeslagM2 <= max
}

// greater then equal P6GrondbeslagM2 filter
func FilterP6GrondbeslagM2gte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6GrondbeslagM2 >= min
}

// contain filter P6GrondbeslagM2
func FilterP6GrondbeslagM2Contains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6GrondbeslagM2)
	return strings.Contains(number, s)
}

// startswith filter P6GrondbeslagM2
func FilterP6GrondbeslagM2StartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6GrondbeslagM2)
	return strings.HasPrefix(number, s)
}

// getter P6GrondbeslagM2
func GettersP6GrondbeslagM2(i *Item) string {
	return fmt.Sprintf("%d", i.P6GrondbeslagM2)
} // match filters P6Gasm32023
func FilterP6Gasm32023Match(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.P6Gasm32023 == eq
}

// less then equal P6Gasm32023 filter
func FilterP6Gasm32023lte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6Gasm32023 <= max
}

// greater then equal P6Gasm32023 filter
func FilterP6Gasm32023gte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6Gasm32023 >= min
}

// contain filter P6Gasm32023
func FilterP6Gasm32023Contains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6Gasm32023)
	return strings.Contains(number, s)
}

// startswith filter P6Gasm32023
func FilterP6Gasm32023StartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6Gasm32023)
	return strings.HasPrefix(number, s)
}

// getter P6Gasm32023
func GettersP6Gasm32023(i *Item) string {
	return fmt.Sprintf("%d", i.P6Gasm32023)
} // match filters P6GasAansluitingen2023
func FilterP6GasAansluitingen2023Match(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.P6GasAansluitingen2023 == eq
}

// less then equal P6GasAansluitingen2023 filter
func FilterP6GasAansluitingen2023lte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6GasAansluitingen2023 <= max
}

// greater then equal P6GasAansluitingen2023 filter
func FilterP6GasAansluitingen2023gte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6GasAansluitingen2023 >= min
}

// contain filter P6GasAansluitingen2023
func FilterP6GasAansluitingen2023Contains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6GasAansluitingen2023)
	return strings.Contains(number, s)
}

// startswith filter P6GasAansluitingen2023
func FilterP6GasAansluitingen2023StartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6GasAansluitingen2023)
	return strings.HasPrefix(number, s)
}

// getter P6GasAansluitingen2023
func GettersP6GasAansluitingen2023(i *Item) string {
	return fmt.Sprintf("%d", i.P6GasAansluitingen2023)
} // match filters P6Kwh2023
func FilterP6Kwh2023Match(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.P6Kwh2023 == eq
}

// less then equal P6Kwh2023 filter
func FilterP6Kwh2023lte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6Kwh2023 <= max
}

// greater then equal P6Kwh2023 filter
func FilterP6Kwh2023gte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6Kwh2023 >= min
}

// contain filter P6Kwh2023
func FilterP6Kwh2023Contains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6Kwh2023)
	return strings.Contains(number, s)
}

// startswith filter P6Kwh2023
func FilterP6Kwh2023StartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6Kwh2023)
	return strings.HasPrefix(number, s)
}

// getter P6Kwh2023
func GettersP6Kwh2023(i *Item) string {
	return fmt.Sprintf("%d", i.P6Kwh2023)
} // match filters P6KwhProductie2023
func FilterP6KwhProductie2023Match(i *Item, s string) bool {
	eq, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}

	return i.P6KwhProductie2023 == eq
}

// less then equal P6KwhProductie2023 filter
func FilterP6KwhProductie2023lte(i *Item, s string) bool {
	max, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6KwhProductie2023 <= max
}

// greater then equal P6KwhProductie2023 filter
func FilterP6KwhProductie2023gte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	return i.P6KwhProductie2023 >= min
}

// contain filter P6KwhProductie2023
func FilterP6KwhProductie2023Contains(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6KwhProductie2023)
	return strings.Contains(number, s)
}

// startswith filter P6KwhProductie2023
func FilterP6KwhProductie2023StartsWith(i *Item, s string) bool {
	number := fmt.Sprintf("%d", i.P6KwhProductie2023)
	return strings.HasPrefix(number, s)
}

// getter P6KwhProductie2023
func GettersP6KwhProductie2023(i *Item) string {
	return fmt.Sprintf("%d", i.P6KwhProductie2023)
}

// contain filter Point
func FilterPointContains(i *Item, s string) bool {
	return strings.Contains(i.Point, s)
}

// startswith filter Point
func FilterPointStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Point, s)
}

// match filters Point
func FilterPointMatch(i *Item, s string) bool {
	return i.Point == s
}

// gte filters Point
func FilterPointgte(i *Item, s string) bool {
	return i.Point <= s
}

// lte filters Point
func FilterPointlte(i *Item, s string) bool {
	return i.Point <= s
}

// getter Point
func GettersPoint(i *Item) string {
	return i.Point
}

// contain filter Buurtcode
func FilterBuurtcodeContains(i *Item, s string) bool {
	return strings.Contains(Buurtcode.GetValue(i.Buurtcode), s)
}

// startswith filter Buurtcode
func FilterBuurtcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurtcode.GetValue(i.Buurtcode), s)
}

// match filters Buurtcode
func FilterBuurtcodeMatch(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) == s
}

// gte filters Buurtcode
func FilterBuurtcodegte(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) <= s
}

// lte filters Buurtcode
func FilterBuurtcodelte(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) <= s
}

// getter Buurtcode
func GettersBuurtcode(i *Item) string {
	return Buurtcode.GetValue(i.Buurtcode)
}

// contain filter Buurtnaam
func FilterBuurtnaamContains(i *Item, s string) bool {
	return strings.Contains(Buurtnaam.GetValue(i.Buurtnaam), s)
}

// startswith filter Buurtnaam
func FilterBuurtnaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurtnaam.GetValue(i.Buurtnaam), s)
}

// match filters Buurtnaam
func FilterBuurtnaamMatch(i *Item, s string) bool {
	return Buurtnaam.GetValue(i.Buurtnaam) == s
}

// gte filters Buurtnaam
func FilterBuurtnaamgte(i *Item, s string) bool {
	return Buurtnaam.GetValue(i.Buurtnaam) <= s
}

// lte filters Buurtnaam
func FilterBuurtnaamlte(i *Item, s string) bool {
	return Buurtnaam.GetValue(i.Buurtnaam) <= s
}

// getter Buurtnaam
func GettersBuurtnaam(i *Item) string {
	return Buurtnaam.GetValue(i.Buurtnaam)
}

// contain filter Wijkcode
func FilterWijkcodeContains(i *Item, s string) bool {
	return strings.Contains(Wijkcode.GetValue(i.Wijkcode), s)
}

// startswith filter Wijkcode
func FilterWijkcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Wijkcode.GetValue(i.Wijkcode), s)
}

// match filters Wijkcode
func FilterWijkcodeMatch(i *Item, s string) bool {
	return Wijkcode.GetValue(i.Wijkcode) == s
}

// gte filters Wijkcode
func FilterWijkcodegte(i *Item, s string) bool {
	return Wijkcode.GetValue(i.Wijkcode) <= s
}

// lte filters Wijkcode
func FilterWijkcodelte(i *Item, s string) bool {
	return Wijkcode.GetValue(i.Wijkcode) <= s
}

// getter Wijkcode
func GettersWijkcode(i *Item) string {
	return Wijkcode.GetValue(i.Wijkcode)
}

// contain filter Wijknaam
func FilterWijknaamContains(i *Item, s string) bool {
	return strings.Contains(Wijknaam.GetValue(i.Wijknaam), s)
}

// startswith filter Wijknaam
func FilterWijknaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Wijknaam.GetValue(i.Wijknaam), s)
}

// match filters Wijknaam
func FilterWijknaamMatch(i *Item, s string) bool {
	return Wijknaam.GetValue(i.Wijknaam) == s
}

// gte filters Wijknaam
func FilterWijknaamgte(i *Item, s string) bool {
	return Wijknaam.GetValue(i.Wijknaam) <= s
}

// lte filters Wijknaam
func FilterWijknaamlte(i *Item, s string) bool {
	return Wijknaam.GetValue(i.Wijknaam) <= s
}

// getter Wijknaam
func GettersWijknaam(i *Item) string {
	return Wijknaam.GetValue(i.Wijknaam)
}

// contain filter Gemeentecode
func FilterGemeentecodeContains(i *Item, s string) bool {
	return strings.Contains(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// startswith filter Gemeentecode
func FilterGemeentecodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// match filters Gemeentecode
func FilterGemeentecodeMatch(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) == s
}

// gte filters Gemeentecode
func FilterGemeentecodegte(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) <= s
}

// lte filters Gemeentecode
func FilterGemeentecodelte(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) <= s
}

// getter Gemeentecode
func GettersGemeentecode(i *Item) string {
	return Gemeentecode.GetValue(i.Gemeentecode)
}

// contain filter Gemeentenaam
func FilterGemeentenaamContains(i *Item, s string) bool {
	return strings.Contains(Gemeentenaam.GetValue(i.Gemeentenaam), s)
}

// startswith filter Gemeentenaam
func FilterGemeentenaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeentenaam.GetValue(i.Gemeentenaam), s)
}

// match filters Gemeentenaam
func FilterGemeentenaamMatch(i *Item, s string) bool {
	return Gemeentenaam.GetValue(i.Gemeentenaam) == s
}

// gte filters Gemeentenaam
func FilterGemeentenaamgte(i *Item, s string) bool {
	return Gemeentenaam.GetValue(i.Gemeentenaam) <= s
}

// lte filters Gemeentenaam
func FilterGemeentenaamlte(i *Item, s string) bool {
	return Gemeentenaam.GetValue(i.Gemeentenaam) <= s
}

// getter Gemeentenaam
func GettersGemeentenaam(i *Item) string {
	return Gemeentenaam.GetValue(i.Gemeentenaam)
}

// contain filter Provincienaam
func FilterProvincienaamContains(i *Item, s string) bool {
	return strings.Contains(Provincienaam.GetValue(i.Provincienaam), s)
}

// startswith filter Provincienaam
func FilterProvincienaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Provincienaam.GetValue(i.Provincienaam), s)
}

// match filters Provincienaam
func FilterProvincienaamMatch(i *Item, s string) bool {
	return Provincienaam.GetValue(i.Provincienaam) == s
}

// gte filters Provincienaam
func FilterProvincienaamgte(i *Item, s string) bool {
	return Provincienaam.GetValue(i.Provincienaam) <= s
}

// lte filters Provincienaam
func FilterProvincienaamlte(i *Item, s string) bool {
	return Provincienaam.GetValue(i.Provincienaam) <= s
}

// getter Provincienaam
func GettersProvincienaam(i *Item) string {
	return Provincienaam.GetValue(i.Provincienaam)
}

// contain filter Provinciecode
func FilterProvinciecodeContains(i *Item, s string) bool {
	return strings.Contains(Provinciecode.GetValue(i.Provinciecode), s)
}

// startswith filter Provinciecode
func FilterProvinciecodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Provinciecode.GetValue(i.Provinciecode), s)
}

// match filters Provinciecode
func FilterProvinciecodeMatch(i *Item, s string) bool {
	return Provinciecode.GetValue(i.Provinciecode) == s
}

// gte filters Provinciecode
func FilterProvinciecodegte(i *Item, s string) bool {
	return Provinciecode.GetValue(i.Provinciecode) <= s
}

// lte filters Provinciecode
func FilterProvinciecodelte(i *Item, s string) bool {
	return Provinciecode.GetValue(i.Provinciecode) <= s
}

// getter Provinciecode
func GettersProvinciecode(i *Item) string {
	return Provinciecode.GetValue(i.Provinciecode)
}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			filterKey := f + "-" + c
			if _, ok := RegisterFuncMap[filterKey]; !ok {
				fmt.Println("missing in RegisterMap: " + filterKey)
			}
		}
	}
}

func FilterGeneralStartsWith(i *Item, s string) bool {
	return FilterPidStartsWith(i, s) ||
		FilterPostcodeStartsWith(i, s) ||
		FilterStraatStartsWith(i, s) ||
		FilterWoonplaatsnaamStartsWith(i, s) ||
		FilterHuisletterStartsWith(i, s) ||
		FilterHuisnummertoevoegingStartsWith(i, s) ||
		FilterGebruiksdoelenStartsWith(i, s) ||
		FilterNetbeheerderStartsWith(i, s) ||
		FilterEnergieklasseStartsWith(i, s) ||
		FilterWoningTypeStartsWith(i, s) ||
		FilterSbicodeStartsWith(i, s) ||
		FilterPointStartsWith(i, s) ||
		FilterBuurtcodeStartsWith(i, s) ||
		FilterBuurtnaamStartsWith(i, s) ||
		FilterWijkcodeStartsWith(i, s) ||
		FilterWijknaamStartsWith(i, s) ||
		FilterGemeentecodeStartsWith(i, s) ||
		FilterGemeentenaamStartsWith(i, s) ||
		FilterProvincienaamStartsWith(i, s) ||
		FilterProvinciecodeStartsWith(i, s)

}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	RegisterFuncMap["search"] = FilterGeneralStartsWith

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	// example RegisterGetters["value"] = GettersEkey

	// register filters

	//register filters for Numid
	RegisterFuncMap["match-numid"] = FilterNumidMatch
	RegisterFuncMap["contains-numid"] = FilterNumidContains
	RegisterFuncMap["startswith-numid"] = FilterNumidStartsWith
	RegisterFuncMap["gte-numid"] = FilterNumidgte
	RegisterFuncMap["lte-numid"] = FilterNumidlte
	RegisterGetters["numid"] = GettersNumid
	RegisterGroupBy["numid"] = GettersNumid

	//register filters for Pid
	RegisterFuncMap["match-pid"] = FilterPidMatch
	RegisterFuncMap["contains-pid"] = FilterPidContains
	RegisterFuncMap["startswith-pid"] = FilterPidStartsWith
	RegisterFuncMap["gte-pid"] = FilterPidgte
	RegisterFuncMap["lte-pid"] = FilterPidlte
	RegisterGetters["pid"] = GettersPid
	RegisterGroupBy["pid"] = GettersPid

	//register filters for Vid
	RegisterFuncMap["match-vid"] = FilterVidMatch
	RegisterFuncMap["contains-vid"] = FilterVidContains
	RegisterFuncMap["startswith-vid"] = FilterVidStartsWith
	RegisterFuncMap["gte-vid"] = FilterVidgte
	RegisterFuncMap["lte-vid"] = FilterVidlte
	RegisterGetters["vid"] = GettersVid
	RegisterGroupBy["vid"] = GettersVid

	//register filters for Lid
	RegisterFuncMap["match-lid"] = FilterLidMatch
	RegisterFuncMap["contains-lid"] = FilterLidContains
	RegisterFuncMap["startswith-lid"] = FilterLidStartsWith
	RegisterFuncMap["gte-lid"] = FilterLidgte
	RegisterFuncMap["lte-lid"] = FilterLidlte
	RegisterGetters["lid"] = GettersLid
	RegisterGroupBy["lid"] = GettersLid

	//register filters for Sid
	RegisterFuncMap["match-sid"] = FilterSidMatch
	RegisterFuncMap["contains-sid"] = FilterSidContains
	RegisterFuncMap["startswith-sid"] = FilterSidStartsWith
	RegisterFuncMap["gte-sid"] = FilterSidgte
	RegisterFuncMap["lte-sid"] = FilterSidlte
	RegisterGetters["sid"] = GettersSid
	RegisterGroupBy["sid"] = GettersSid

	//register filters for Postcode
	RegisterFuncMap["match-postcode"] = FilterPostcodeMatch
	RegisterFuncMap["contains-postcode"] = FilterPostcodeContains
	RegisterFuncMap["startswith-postcode"] = FilterPostcodeStartsWith
	RegisterFuncMap["gte-postcode"] = FilterPostcodegte
	RegisterFuncMap["lte-postcode"] = FilterPostcodelte
	RegisterGetters["postcode"] = GettersPostcode
	RegisterGroupBy["postcode"] = GettersPostcode

	//register filters for Straat
	RegisterFuncMap["match-straat"] = FilterStraatMatch
	RegisterFuncMap["contains-straat"] = FilterStraatContains
	RegisterFuncMap["startswith-straat"] = FilterStraatStartsWith
	RegisterFuncMap["gte-straat"] = FilterStraatgte
	RegisterFuncMap["lte-straat"] = FilterStraatlte
	RegisterGetters["straat"] = GettersStraat
	RegisterGroupBy["straat"] = GettersStraat

	//register filters for Woonplaatsnaam
	RegisterFuncMap["match-woonplaatsnaam"] = FilterWoonplaatsnaamMatch
	RegisterFuncMap["contains-woonplaatsnaam"] = FilterWoonplaatsnaamContains
	RegisterFuncMap["startswith-woonplaatsnaam"] = FilterWoonplaatsnaamStartsWith
	RegisterFuncMap["gte-woonplaatsnaam"] = FilterWoonplaatsnaamgte
	RegisterFuncMap["lte-woonplaatsnaam"] = FilterWoonplaatsnaamlte
	RegisterGetters["woonplaatsnaam"] = GettersWoonplaatsnaam
	RegisterGroupBy["woonplaatsnaam"] = GettersWoonplaatsnaam

	//register filters for Huisnummer
	RegisterFuncMap["match-huisnummer"] = FilterHuisnummerMatch
	RegisterFuncMap["contains-huisnummer"] = FilterHuisnummerContains
	RegisterFuncMap["startswith-huisnummer"] = FilterHuisnummerStartsWith
	RegisterFuncMap["gte-huisnummer"] = FilterHuisnummergte
	RegisterFuncMap["lte-huisnummer"] = FilterHuisnummerlte
	RegisterGetters["huisnummer"] = GettersHuisnummer
	RegisterGroupBy["huisnummer"] = GettersHuisnummer

	//register filters for Huisletter
	RegisterFuncMap["match-huisletter"] = FilterHuisletterMatch
	RegisterFuncMap["contains-huisletter"] = FilterHuisletterContains
	RegisterFuncMap["startswith-huisletter"] = FilterHuisletterStartsWith
	RegisterFuncMap["gte-huisletter"] = FilterHuislettergte
	RegisterFuncMap["lte-huisletter"] = FilterHuisletterlte
	RegisterGetters["huisletter"] = GettersHuisletter
	RegisterGroupBy["huisletter"] = GettersHuisletter

	//register filters for Huisnummertoevoeging
	RegisterFuncMap["match-huisnummertoevoeging"] = FilterHuisnummertoevoegingMatch
	RegisterFuncMap["contains-huisnummertoevoeging"] = FilterHuisnummertoevoegingContains
	RegisterFuncMap["startswith-huisnummertoevoeging"] = FilterHuisnummertoevoegingStartsWith
	RegisterFuncMap["gte-huisnummertoevoeging"] = FilterHuisnummertoevoeginggte
	RegisterFuncMap["lte-huisnummertoevoeging"] = FilterHuisnummertoevoeginglte
	RegisterGetters["huisnummertoevoeging"] = GettersHuisnummertoevoeging
	RegisterGroupBy["huisnummertoevoeging"] = GettersHuisnummertoevoeging

	//register filters for Oppervlakte
	RegisterFuncMap["match-oppervlakte"] = FilterOppervlakteMatch
	RegisterFuncMap["contains-oppervlakte"] = FilterOppervlakteContains
	RegisterFuncMap["startswith-oppervlakte"] = FilterOppervlakteStartsWith
	RegisterFuncMap["gte-oppervlakte"] = FilterOppervlaktegte
	RegisterFuncMap["lte-oppervlakte"] = FilterOppervlaktelte
	RegisterGetters["oppervlakte"] = GettersOppervlakte
	RegisterGroupBy["oppervlakte"] = GettersOppervlakte

	//register filters for Woningequivalent
	RegisterFuncMap["match-woningequivalent"] = FilterWoningequivalentMatch
	RegisterFuncMap["contains-woningequivalent"] = FilterWoningequivalentContains
	RegisterFuncMap["startswith-woningequivalent"] = FilterWoningequivalentStartsWith
	RegisterFuncMap["gte-woningequivalent"] = FilterWoningequivalentgte
	RegisterFuncMap["lte-woningequivalent"] = FilterWoningequivalentlte
	RegisterGetters["woningequivalent"] = GettersWoningequivalent
	RegisterGroupBy["woningequivalent"] = GettersWoningequivalent

	//register filters for Gebruiksdoelen
	RegisterFuncMap["match-gebruiksdoelen"] = FilterGebruiksdoelenMatch
	RegisterFuncMap["contains-gebruiksdoelen"] = FilterGebruiksdoelenContains
	RegisterFuncMap["startswith-gebruiksdoelen"] = FilterGebruiksdoelenStartsWith
	RegisterFuncMap["gte-gebruiksdoelen"] = FilterGebruiksdoelengte
	RegisterFuncMap["lte-gebruiksdoelen"] = FilterGebruiksdoelenlte
	RegisterGetters["gebruiksdoelen"] = GettersGebruiksdoelen
	RegisterGroupBy["gebruiksdoelen"] = GettersGebruiksdoelen

	//register filters for PandBouwjaar
	RegisterFuncMap["match-pand_bouwjaar"] = FilterPandBouwjaarMatch
	RegisterFuncMap["contains-pand_bouwjaar"] = FilterPandBouwjaarContains
	RegisterFuncMap["startswith-pand_bouwjaar"] = FilterPandBouwjaarStartsWith
	RegisterFuncMap["gte-pand_bouwjaar"] = FilterPandBouwjaargte
	RegisterFuncMap["lte-pand_bouwjaar"] = FilterPandBouwjaarlte
	RegisterGetters["pand_bouwjaar"] = GettersPandBouwjaar
	RegisterGroupBy["pand_bouwjaar"] = GettersPandBouwjaar

	//register filters for Pc6GemiddeldeWozWaardeWoning
	RegisterFuncMap["match-pc6_gemiddelde_woz_waarde_woning"] = FilterPc6GemiddeldeWozWaardeWoningMatch
	RegisterFuncMap["contains-pc6_gemiddelde_woz_waarde_woning"] = FilterPc6GemiddeldeWozWaardeWoningContains
	RegisterFuncMap["startswith-pc6_gemiddelde_woz_waarde_woning"] = FilterPc6GemiddeldeWozWaardeWoningStartsWith
	RegisterFuncMap["gte-pc6_gemiddelde_woz_waarde_woning"] = FilterPc6GemiddeldeWozWaardeWoninggte
	RegisterFuncMap["lte-pc6_gemiddelde_woz_waarde_woning"] = FilterPc6GemiddeldeWozWaardeWoninglte
	RegisterGetters["pc6_gemiddelde_woz_waarde_woning"] = GettersPc6GemiddeldeWozWaardeWoning
	RegisterGroupBy["pc6_gemiddelde_woz_waarde_woning"] = GettersPc6GemiddeldeWozWaardeWoning

	//register filters for GemiddeldeGemeenteWoz
	RegisterFuncMap["match-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozMatch
	RegisterFuncMap["contains-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozContains
	RegisterFuncMap["startswith-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozStartsWith
	RegisterFuncMap["gte-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozgte
	RegisterFuncMap["lte-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozlte
	RegisterGetters["gemiddelde_gemeente_woz"] = GettersGemiddeldeGemeenteWoz
	RegisterGroupBy["gemiddelde_gemeente_woz"] = GettersGemiddeldeGemeenteWoz

	//register filters for Pc6EigendomssituatiePercKoop
	RegisterFuncMap["match-pc6_eigendomssituatie_perc_koop"] = FilterPc6EigendomssituatiePercKoopMatch
	RegisterFuncMap["contains-pc6_eigendomssituatie_perc_koop"] = FilterPc6EigendomssituatiePercKoopContains
	RegisterFuncMap["startswith-pc6_eigendomssituatie_perc_koop"] = FilterPc6EigendomssituatiePercKoopStartsWith
	RegisterFuncMap["gte-pc6_eigendomssituatie_perc_koop"] = FilterPc6EigendomssituatiePercKoopgte
	RegisterFuncMap["lte-pc6_eigendomssituatie_perc_koop"] = FilterPc6EigendomssituatiePercKooplte
	RegisterGetters["pc6_eigendomssituatie_perc_koop"] = GettersPc6EigendomssituatiePercKoop
	RegisterGroupBy["pc6_eigendomssituatie_perc_koop"] = GettersPc6EigendomssituatiePercKoop

	//register filters for Pc6EigendomssituatiePercHuur
	RegisterFuncMap["match-pc6_eigendomssituatie_perc_huur"] = FilterPc6EigendomssituatiePercHuurMatch
	RegisterFuncMap["contains-pc6_eigendomssituatie_perc_huur"] = FilterPc6EigendomssituatiePercHuurContains
	RegisterFuncMap["startswith-pc6_eigendomssituatie_perc_huur"] = FilterPc6EigendomssituatiePercHuurStartsWith
	RegisterFuncMap["gte-pc6_eigendomssituatie_perc_huur"] = FilterPc6EigendomssituatiePercHuurgte
	RegisterFuncMap["lte-pc6_eigendomssituatie_perc_huur"] = FilterPc6EigendomssituatiePercHuurlte
	RegisterGetters["pc6_eigendomssituatie_perc_huur"] = GettersPc6EigendomssituatiePercHuur
	RegisterGroupBy["pc6_eigendomssituatie_perc_huur"] = GettersPc6EigendomssituatiePercHuur

	//register filters for Pc6EigendomssituatieAantalWoningenCorporaties
	RegisterFuncMap["match-pc6_eigendomssituatie_aantal_woningen_corporaties"] = FilterPc6EigendomssituatieAantalWoningenCorporatiesMatch
	RegisterFuncMap["contains-pc6_eigendomssituatie_aantal_woningen_corporaties"] = FilterPc6EigendomssituatieAantalWoningenCorporatiesContains
	RegisterFuncMap["startswith-pc6_eigendomssituatie_aantal_woningen_corporaties"] = FilterPc6EigendomssituatieAantalWoningenCorporatiesStartsWith
	RegisterFuncMap["gte-pc6_eigendomssituatie_aantal_woningen_corporaties"] = FilterPc6EigendomssituatieAantalWoningenCorporatiesgte
	RegisterFuncMap["lte-pc6_eigendomssituatie_aantal_woningen_corporaties"] = FilterPc6EigendomssituatieAantalWoningenCorporatieslte
	RegisterGetters["pc6_eigendomssituatie_aantal_woningen_corporaties"] = GettersPc6EigendomssituatieAantalWoningenCorporaties
	RegisterGroupBy["pc6_eigendomssituatie_aantal_woningen_corporaties"] = GettersPc6EigendomssituatieAantalWoningenCorporaties

	//register filters for Netbeheerder
	RegisterFuncMap["match-netbeheerder"] = FilterNetbeheerderMatch
	RegisterFuncMap["contains-netbeheerder"] = FilterNetbeheerderContains
	RegisterFuncMap["startswith-netbeheerder"] = FilterNetbeheerderStartsWith
	RegisterFuncMap["gte-netbeheerder"] = FilterNetbeheerdergte
	RegisterFuncMap["lte-netbeheerder"] = FilterNetbeheerderlte
	RegisterGetters["netbeheerder"] = GettersNetbeheerder
	RegisterGroupBy["netbeheerder"] = GettersNetbeheerder

	//register filters for Energieklasse
	RegisterFuncMap["match-energieklasse"] = FilterEnergieklasseMatch
	RegisterFuncMap["contains-energieklasse"] = FilterEnergieklasseContains
	RegisterFuncMap["startswith-energieklasse"] = FilterEnergieklasseStartsWith
	RegisterFuncMap["gte-energieklasse"] = FilterEnergieklassegte
	RegisterFuncMap["lte-energieklasse"] = FilterEnergieklasselte
	RegisterGetters["energieklasse"] = GettersEnergieklasse
	RegisterGroupBy["energieklasse"] = GettersEnergieklasse

	//register filters for WoningType
	RegisterFuncMap["match-woning_type"] = FilterWoningTypeMatch
	RegisterFuncMap["contains-woning_type"] = FilterWoningTypeContains
	RegisterFuncMap["startswith-woning_type"] = FilterWoningTypeStartsWith
	RegisterFuncMap["gte-woning_type"] = FilterWoningTypegte
	RegisterFuncMap["lte-woning_type"] = FilterWoningTypelte
	RegisterGetters["woning_type"] = GettersWoningType
	RegisterGroupBy["woning_type"] = GettersWoningType

	//register filters for Sbicode
	RegisterFuncMap["match-sbicode"] = FilterSbicodeMatch
	RegisterFuncMap["contains-sbicode"] = FilterSbicodeContains
	RegisterFuncMap["startswith-sbicode"] = FilterSbicodeStartsWith
	RegisterFuncMap["gte-sbicode"] = FilterSbicodegte
	RegisterFuncMap["lte-sbicode"] = FilterSbicodelte
	RegisterGetters["sbicode"] = GettersSbicode
	RegisterGroupBy["sbicode"] = GettersSbicode

	//register filters for GasEanCount
	RegisterFuncMap["match-gas_ean_count"] = FilterGasEanCountMatch
	RegisterFuncMap["contains-gas_ean_count"] = FilterGasEanCountContains
	RegisterFuncMap["startswith-gas_ean_count"] = FilterGasEanCountStartsWith
	RegisterFuncMap["gte-gas_ean_count"] = FilterGasEanCountgte
	RegisterFuncMap["lte-gas_ean_count"] = FilterGasEanCountlte
	RegisterGetters["gas_ean_count"] = GettersGasEanCount
	RegisterGroupBy["gas_ean_count"] = GettersGasEanCount

	//register filters for P6GrondbeslagM2
	RegisterFuncMap["match-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2Match
	RegisterFuncMap["contains-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2Contains
	RegisterFuncMap["startswith-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2StartsWith
	RegisterFuncMap["gte-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2gte
	RegisterFuncMap["lte-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2lte
	RegisterGetters["p6_grondbeslag_m2"] = GettersP6GrondbeslagM2
	RegisterGroupBy["p6_grondbeslag_m2"] = GettersP6GrondbeslagM2

	//register filters for P6Gasm32023
	RegisterFuncMap["match-p6_gasm3_2023"] = FilterP6Gasm32023Match
	RegisterFuncMap["contains-p6_gasm3_2023"] = FilterP6Gasm32023Contains
	RegisterFuncMap["startswith-p6_gasm3_2023"] = FilterP6Gasm32023StartsWith
	RegisterFuncMap["gte-p6_gasm3_2023"] = FilterP6Gasm32023gte
	RegisterFuncMap["lte-p6_gasm3_2023"] = FilterP6Gasm32023lte
	RegisterGetters["p6_gasm3_2023"] = GettersP6Gasm32023
	RegisterGroupBy["p6_gasm3_2023"] = GettersP6Gasm32023

	//register filters for P6GasAansluitingen2023
	RegisterFuncMap["match-p6_gas_aansluitingen_2023"] = FilterP6GasAansluitingen2023Match
	RegisterFuncMap["contains-p6_gas_aansluitingen_2023"] = FilterP6GasAansluitingen2023Contains
	RegisterFuncMap["startswith-p6_gas_aansluitingen_2023"] = FilterP6GasAansluitingen2023StartsWith
	RegisterFuncMap["gte-p6_gas_aansluitingen_2023"] = FilterP6GasAansluitingen2023gte
	RegisterFuncMap["lte-p6_gas_aansluitingen_2023"] = FilterP6GasAansluitingen2023lte
	RegisterGetters["p6_gas_aansluitingen_2023"] = GettersP6GasAansluitingen2023
	RegisterGroupBy["p6_gas_aansluitingen_2023"] = GettersP6GasAansluitingen2023

	//register filters for P6Kwh2023
	RegisterFuncMap["match-p6_kwh_2023"] = FilterP6Kwh2023Match
	RegisterFuncMap["contains-p6_kwh_2023"] = FilterP6Kwh2023Contains
	RegisterFuncMap["startswith-p6_kwh_2023"] = FilterP6Kwh2023StartsWith
	RegisterFuncMap["gte-p6_kwh_2023"] = FilterP6Kwh2023gte
	RegisterFuncMap["lte-p6_kwh_2023"] = FilterP6Kwh2023lte
	RegisterGetters["p6_kwh_2023"] = GettersP6Kwh2023
	RegisterGroupBy["p6_kwh_2023"] = GettersP6Kwh2023

	//register filters for P6KwhProductie2023
	RegisterFuncMap["match-p6_kwh_productie_2023"] = FilterP6KwhProductie2023Match
	RegisterFuncMap["contains-p6_kwh_productie_2023"] = FilterP6KwhProductie2023Contains
	RegisterFuncMap["startswith-p6_kwh_productie_2023"] = FilterP6KwhProductie2023StartsWith
	RegisterFuncMap["gte-p6_kwh_productie_2023"] = FilterP6KwhProductie2023gte
	RegisterFuncMap["lte-p6_kwh_productie_2023"] = FilterP6KwhProductie2023lte
	RegisterGetters["p6_kwh_productie_2023"] = GettersP6KwhProductie2023
	RegisterGroupBy["p6_kwh_productie_2023"] = GettersP6KwhProductie2023

	//register filters for Point
	RegisterFuncMap["match-point"] = FilterPointMatch
	RegisterFuncMap["contains-point"] = FilterPointContains
	RegisterFuncMap["startswith-point"] = FilterPointStartsWith
	RegisterFuncMap["gte-point"] = FilterPointgte
	RegisterFuncMap["lte-point"] = FilterPointlte
	RegisterGetters["point"] = GettersPoint
	RegisterGroupBy["point"] = GettersPoint

	//register filters for Buurtcode
	RegisterFuncMap["match-buurtcode"] = FilterBuurtcodeMatch
	RegisterFuncMap["contains-buurtcode"] = FilterBuurtcodeContains
	RegisterFuncMap["startswith-buurtcode"] = FilterBuurtcodeStartsWith
	RegisterFuncMap["gte-buurtcode"] = FilterBuurtcodegte
	RegisterFuncMap["lte-buurtcode"] = FilterBuurtcodelte
	RegisterGetters["buurtcode"] = GettersBuurtcode
	RegisterGroupBy["buurtcode"] = GettersBuurtcode

	//register filters for Buurtnaam
	RegisterFuncMap["match-buurtnaam"] = FilterBuurtnaamMatch
	RegisterFuncMap["contains-buurtnaam"] = FilterBuurtnaamContains
	RegisterFuncMap["startswith-buurtnaam"] = FilterBuurtnaamStartsWith
	RegisterFuncMap["gte-buurtnaam"] = FilterBuurtnaamgte
	RegisterFuncMap["lte-buurtnaam"] = FilterBuurtnaamlte
	RegisterGetters["buurtnaam"] = GettersBuurtnaam
	RegisterGroupBy["buurtnaam"] = GettersBuurtnaam

	//register filters for Wijkcode
	RegisterFuncMap["match-wijkcode"] = FilterWijkcodeMatch
	RegisterFuncMap["contains-wijkcode"] = FilterWijkcodeContains
	RegisterFuncMap["startswith-wijkcode"] = FilterWijkcodeStartsWith
	RegisterFuncMap["gte-wijkcode"] = FilterWijkcodegte
	RegisterFuncMap["lte-wijkcode"] = FilterWijkcodelte
	RegisterGetters["wijkcode"] = GettersWijkcode
	RegisterGroupBy["wijkcode"] = GettersWijkcode

	//register filters for Wijknaam
	RegisterFuncMap["match-wijknaam"] = FilterWijknaamMatch
	RegisterFuncMap["contains-wijknaam"] = FilterWijknaamContains
	RegisterFuncMap["startswith-wijknaam"] = FilterWijknaamStartsWith
	RegisterFuncMap["gte-wijknaam"] = FilterWijknaamgte
	RegisterFuncMap["lte-wijknaam"] = FilterWijknaamlte
	RegisterGetters["wijknaam"] = GettersWijknaam
	RegisterGroupBy["wijknaam"] = GettersWijknaam

	//register filters for Gemeentecode
	RegisterFuncMap["match-gemeentecode"] = FilterGemeentecodeMatch
	RegisterFuncMap["contains-gemeentecode"] = FilterGemeentecodeContains
	RegisterFuncMap["startswith-gemeentecode"] = FilterGemeentecodeStartsWith
	RegisterFuncMap["gte-gemeentecode"] = FilterGemeentecodegte
	RegisterFuncMap["lte-gemeentecode"] = FilterGemeentecodelte
	RegisterGetters["gemeentecode"] = GettersGemeentecode
	RegisterGroupBy["gemeentecode"] = GettersGemeentecode

	//register filters for Gemeentenaam
	RegisterFuncMap["match-gemeentenaam"] = FilterGemeentenaamMatch
	RegisterFuncMap["contains-gemeentenaam"] = FilterGemeentenaamContains
	RegisterFuncMap["startswith-gemeentenaam"] = FilterGemeentenaamStartsWith
	RegisterFuncMap["gte-gemeentenaam"] = FilterGemeentenaamgte
	RegisterFuncMap["lte-gemeentenaam"] = FilterGemeentenaamlte
	RegisterGetters["gemeentenaam"] = GettersGemeentenaam
	RegisterGroupBy["gemeentenaam"] = GettersGemeentenaam

	//register filters for Provincienaam
	RegisterFuncMap["match-provincienaam"] = FilterProvincienaamMatch
	RegisterFuncMap["contains-provincienaam"] = FilterProvincienaamContains
	RegisterFuncMap["startswith-provincienaam"] = FilterProvincienaamStartsWith
	RegisterFuncMap["gte-provincienaam"] = FilterProvincienaamgte
	RegisterFuncMap["lte-provincienaam"] = FilterProvincienaamlte
	RegisterGetters["provincienaam"] = GettersProvincienaam
	RegisterGroupBy["provincienaam"] = GettersProvincienaam

	//register filters for Provinciecode
	RegisterFuncMap["match-provinciecode"] = FilterProvinciecodeMatch
	RegisterFuncMap["contains-provinciecode"] = FilterProvinciecodeContains
	RegisterFuncMap["startswith-provinciecode"] = FilterProvinciecodeStartsWith
	RegisterFuncMap["gte-provinciecode"] = FilterProvinciecodegte
	RegisterFuncMap["lte-provinciecode"] = FilterProvinciecodelte
	RegisterGetters["provinciecode"] = GettersProvinciecode
	RegisterGroupBy["provinciecode"] = GettersProvinciecode

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount

	// custom
	RegisterReduce["woningequivalent"] = reduceWEQ
	RegisterReduce["eancodes"] = reduceEAN

}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"numid":  func(i, j int) bool { return items[i].Numid < items[j].Numid },
		"-numid": func(i, j int) bool { return items[i].Numid > items[j].Numid },

		"pid":  func(i, j int) bool { return Pid.GetValue(items[i].Pid) < Pid.GetValue(items[j].Pid) },
		"-pid": func(i, j int) bool { return Pid.GetValue(items[i].Pid) > Pid.GetValue(items[j].Pid) },

		"vid":  func(i, j int) bool { return items[i].Vid < items[j].Vid },
		"-vid": func(i, j int) bool { return items[i].Vid > items[j].Vid },

		"lid":  func(i, j int) bool { return items[i].Lid < items[j].Lid },
		"-lid": func(i, j int) bool { return items[i].Lid > items[j].Lid },

		"sid":  func(i, j int) bool { return items[i].Sid < items[j].Sid },
		"-sid": func(i, j int) bool { return items[i].Sid > items[j].Sid },

		"postcode": func(i, j int) bool {
			return Postcode.GetValue(items[i].Postcode) < Postcode.GetValue(items[j].Postcode)
		},
		"-postcode": func(i, j int) bool {
			return Postcode.GetValue(items[i].Postcode) > Postcode.GetValue(items[j].Postcode)
		},

		"straat":  func(i, j int) bool { return items[i].Straat < items[j].Straat },
		"-straat": func(i, j int) bool { return items[i].Straat > items[j].Straat },

		"woonplaatsnaam": func(i, j int) bool {
			return Woonplaatsnaam.GetValue(items[i].Woonplaatsnaam) < Woonplaatsnaam.GetValue(items[j].Woonplaatsnaam)
		},
		"-woonplaatsnaam": func(i, j int) bool {
			return Woonplaatsnaam.GetValue(items[i].Woonplaatsnaam) > Woonplaatsnaam.GetValue(items[j].Woonplaatsnaam)
		},

		"huisnummer":  func(i, j int) bool { return items[i].Huisnummer < items[j].Huisnummer },
		"-huisnummer": func(i, j int) bool { return items[i].Huisnummer > items[j].Huisnummer },

		"huisletter": func(i, j int) bool {
			return Huisletter.GetValue(items[i].Huisletter) < Huisletter.GetValue(items[j].Huisletter)
		},
		"-huisletter": func(i, j int) bool {
			return Huisletter.GetValue(items[i].Huisletter) > Huisletter.GetValue(items[j].Huisletter)
		},

		"huisnummertoevoeging": func(i, j int) bool {
			return Huisnummertoevoeging.GetValue(items[i].Huisnummertoevoeging) < Huisnummertoevoeging.GetValue(items[j].Huisnummertoevoeging)
		},
		"-huisnummertoevoeging": func(i, j int) bool {
			return Huisnummertoevoeging.GetValue(items[i].Huisnummertoevoeging) > Huisnummertoevoeging.GetValue(items[j].Huisnummertoevoeging)
		},

		"oppervlakte":  func(i, j int) bool { return items[i].Oppervlakte < items[j].Oppervlakte },
		"-oppervlakte": func(i, j int) bool { return items[i].Oppervlakte > items[j].Oppervlakte },

		"woningequivalent":  func(i, j int) bool { return items[i].Woningequivalent < items[j].Woningequivalent },
		"-woningequivalent": func(i, j int) bool { return items[i].Woningequivalent > items[j].Woningequivalent },

		"gebruiksdoelen": func(i, j int) bool {
			return Gebruiksdoelen.GetArrayValue(items[i].Gebruiksdoelen) < Gebruiksdoelen.GetArrayValue(items[j].Gebruiksdoelen)
		},
		"-gebruiksdoelen": func(i, j int) bool {
			return Gebruiksdoelen.GetArrayValue(items[i].Gebruiksdoelen) > Gebruiksdoelen.GetArrayValue(items[j].Gebruiksdoelen)
		},

		"pand_bouwjaar":  func(i, j int) bool { return items[i].PandBouwjaar < items[j].PandBouwjaar },
		"-pand_bouwjaar": func(i, j int) bool { return items[i].PandBouwjaar > items[j].PandBouwjaar },

		"pc6_gemiddelde_woz_waarde_woning": func(i, j int) bool {
			return items[i].Pc6GemiddeldeWozWaardeWoning < items[j].Pc6GemiddeldeWozWaardeWoning
		},
		"-pc6_gemiddelde_woz_waarde_woning": func(i, j int) bool {
			return items[i].Pc6GemiddeldeWozWaardeWoning > items[j].Pc6GemiddeldeWozWaardeWoning
		},

		"gemiddelde_gemeente_woz":  func(i, j int) bool { return items[i].GemiddeldeGemeenteWoz < items[j].GemiddeldeGemeenteWoz },
		"-gemiddelde_gemeente_woz": func(i, j int) bool { return items[i].GemiddeldeGemeenteWoz > items[j].GemiddeldeGemeenteWoz },

		"pc6_eigendomssituatie_perc_koop": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercKoop < items[j].Pc6EigendomssituatiePercKoop
		},
		"-pc6_eigendomssituatie_perc_koop": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercKoop > items[j].Pc6EigendomssituatiePercKoop
		},

		"pc6_eigendomssituatie_perc_huur": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercHuur < items[j].Pc6EigendomssituatiePercHuur
		},
		"-pc6_eigendomssituatie_perc_huur": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercHuur > items[j].Pc6EigendomssituatiePercHuur
		},

		"pc6_eigendomssituatie_aantal_woningen_corporaties": func(i, j int) bool {
			return items[i].Pc6EigendomssituatieAantalWoningenCorporaties < items[j].Pc6EigendomssituatieAantalWoningenCorporaties
		},
		"-pc6_eigendomssituatie_aantal_woningen_corporaties": func(i, j int) bool {
			return items[i].Pc6EigendomssituatieAantalWoningenCorporaties > items[j].Pc6EigendomssituatieAantalWoningenCorporaties
		},

		"netbeheerder": func(i, j int) bool {
			return Netbeheerder.GetValue(items[i].Netbeheerder) < Netbeheerder.GetValue(items[j].Netbeheerder)
		},
		"-netbeheerder": func(i, j int) bool {
			return Netbeheerder.GetValue(items[i].Netbeheerder) > Netbeheerder.GetValue(items[j].Netbeheerder)
		},

		"energieklasse": func(i, j int) bool {
			return Energieklasse.GetValue(items[i].Energieklasse) < Energieklasse.GetValue(items[j].Energieklasse)
		},
		"-energieklasse": func(i, j int) bool {
			return Energieklasse.GetValue(items[i].Energieklasse) > Energieklasse.GetValue(items[j].Energieklasse)
		},

		"woning_type": func(i, j int) bool {
			return WoningType.GetValue(items[i].WoningType) < WoningType.GetValue(items[j].WoningType)
		},
		"-woning_type": func(i, j int) bool {
			return WoningType.GetValue(items[i].WoningType) > WoningType.GetValue(items[j].WoningType)
		},

		"sbicode":  func(i, j int) bool { return Sbicode.GetValue(items[i].Sbicode) < Sbicode.GetValue(items[j].Sbicode) },
		"-sbicode": func(i, j int) bool { return Sbicode.GetValue(items[i].Sbicode) > Sbicode.GetValue(items[j].Sbicode) },

		"gas_ean_count":  func(i, j int) bool { return items[i].GasEanCount < items[j].GasEanCount },
		"-gas_ean_count": func(i, j int) bool { return items[i].GasEanCount > items[j].GasEanCount },

		"p6_grondbeslag_m2":  func(i, j int) bool { return items[i].P6GrondbeslagM2 < items[j].P6GrondbeslagM2 },
		"-p6_grondbeslag_m2": func(i, j int) bool { return items[i].P6GrondbeslagM2 > items[j].P6GrondbeslagM2 },

		"p6_gasm3_2023":  func(i, j int) bool { return items[i].P6Gasm32023 < items[j].P6Gasm32023 },
		"-p6_gasm3_2023": func(i, j int) bool { return items[i].P6Gasm32023 > items[j].P6Gasm32023 },

		"p6_gas_aansluitingen_2023":  func(i, j int) bool { return items[i].P6GasAansluitingen2023 < items[j].P6GasAansluitingen2023 },
		"-p6_gas_aansluitingen_2023": func(i, j int) bool { return items[i].P6GasAansluitingen2023 > items[j].P6GasAansluitingen2023 },

		"p6_kwh_2023":  func(i, j int) bool { return items[i].P6Kwh2023 < items[j].P6Kwh2023 },
		"-p6_kwh_2023": func(i, j int) bool { return items[i].P6Kwh2023 > items[j].P6Kwh2023 },

		"p6_kwh_productie_2023":  func(i, j int) bool { return items[i].P6KwhProductie2023 < items[j].P6KwhProductie2023 },
		"-p6_kwh_productie_2023": func(i, j int) bool { return items[i].P6KwhProductie2023 > items[j].P6KwhProductie2023 },

		"point":  func(i, j int) bool { return items[i].Point < items[j].Point },
		"-point": func(i, j int) bool { return items[i].Point > items[j].Point },

		"buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) < Buurtcode.GetValue(items[j].Buurtcode)
		},
		"-buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) > Buurtcode.GetValue(items[j].Buurtcode)
		},

		"buurtnaam": func(i, j int) bool {
			return Buurtnaam.GetValue(items[i].Buurtnaam) < Buurtnaam.GetValue(items[j].Buurtnaam)
		},
		"-buurtnaam": func(i, j int) bool {
			return Buurtnaam.GetValue(items[i].Buurtnaam) > Buurtnaam.GetValue(items[j].Buurtnaam)
		},

		"wijkcode": func(i, j int) bool {
			return Wijkcode.GetValue(items[i].Wijkcode) < Wijkcode.GetValue(items[j].Wijkcode)
		},
		"-wijkcode": func(i, j int) bool {
			return Wijkcode.GetValue(items[i].Wijkcode) > Wijkcode.GetValue(items[j].Wijkcode)
		},

		"wijknaam": func(i, j int) bool {
			return Wijknaam.GetValue(items[i].Wijknaam) < Wijknaam.GetValue(items[j].Wijknaam)
		},
		"-wijknaam": func(i, j int) bool {
			return Wijknaam.GetValue(items[i].Wijknaam) > Wijknaam.GetValue(items[j].Wijknaam)
		},

		"gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) < Gemeentecode.GetValue(items[j].Gemeentecode)
		},
		"-gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) > Gemeentecode.GetValue(items[j].Gemeentecode)
		},

		"gemeentenaam": func(i, j int) bool {
			return Gemeentenaam.GetValue(items[i].Gemeentenaam) < Gemeentenaam.GetValue(items[j].Gemeentenaam)
		},
		"-gemeentenaam": func(i, j int) bool {
			return Gemeentenaam.GetValue(items[i].Gemeentenaam) > Gemeentenaam.GetValue(items[j].Gemeentenaam)
		},

		"provincienaam": func(i, j int) bool {
			return Provincienaam.GetValue(items[i].Provincienaam) < Provincienaam.GetValue(items[j].Provincienaam)
		},
		"-provincienaam": func(i, j int) bool {
			return Provincienaam.GetValue(items[i].Provincienaam) > Provincienaam.GetValue(items[j].Provincienaam)
		},

		"provinciecode": func(i, j int) bool {
			return Provinciecode.GetValue(items[i].Provinciecode) < Provinciecode.GetValue(items[j].Provinciecode)
		},
		"-provinciecode": func(i, j int) bool {
			return Provinciecode.GetValue(items[i].Provinciecode) > Provinciecode.GetValue(items[j].Provinciecode)
		},
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
