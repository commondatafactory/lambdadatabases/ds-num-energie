/*
	Transforming ItemsIn -> Items -> ItemsOut
	Where Items has column values stored as integers to save memmory
	maps are needed to restore integers back to the actual string values.
	those are generated and stored here.
*/

package main

type ModelMaps struct {
	Pid                  MappedColumn
	Postcode             MappedColumn
	Woonplaatsnaam       MappedColumn
	Huisletter           MappedColumn
	Huisnummertoevoeging MappedColumn
	Netbeheerder         MappedColumn
	Energieklasse        MappedColumn
	WoningType           MappedColumn
	Sbicode              MappedColumn
	Buurtcode            MappedColumn
	Buurtnaam            MappedColumn
	Wijkcode             MappedColumn
	Wijknaam             MappedColumn
	Gemeentecode         MappedColumn
	Gemeentenaam         MappedColumn
	Provincienaam        MappedColumn
	Provinciecode        MappedColumn
}

var BitArrays map[string]fieldBitarrayMap

var Pid MappedColumn
var Postcode MappedColumn
var Woonplaatsnaam MappedColumn
var Huisletter MappedColumn
var Huisnummertoevoeging MappedColumn
var Netbeheerder MappedColumn
var Energieklasse MappedColumn
var WoningType MappedColumn
var Sbicode MappedColumn
var Buurtcode MappedColumn
var Buurtnaam MappedColumn
var Wijkcode MappedColumn
var Wijknaam MappedColumn
var Gemeentecode MappedColumn
var Gemeentenaam MappedColumn
var Provincienaam MappedColumn
var Provinciecode MappedColumn
var Gebruiksdoelen MappedColumn

func clearBitArrays() {
	BitArrays = make(map[string]fieldBitarrayMap)
}

func init() {
	clearBitArrays()
	setUpRepeatedColumns()
}

func setUpRepeatedColumns() {
	Pid = NewReapeatedColumn("pid")
	Postcode = NewReapeatedColumn("postcode")
	Woonplaatsnaam = NewReapeatedColumn("woonplaatsnaam")
	Huisletter = NewReapeatedColumn("huisletter")
	Huisnummertoevoeging = NewReapeatedColumn("huisnummertoevoeging")
	Netbeheerder = NewReapeatedColumn("netbeheerder")
	Energieklasse = NewReapeatedColumn("energieklasse")
	WoningType = NewReapeatedColumn("woning_type")
	Sbicode = NewReapeatedColumn("sbicode")
	Buurtcode = NewReapeatedColumn("buurtcode")
	Buurtnaam = NewReapeatedColumn("buurtnaam")
	Wijkcode = NewReapeatedColumn("wijkcode")
	Wijknaam = NewReapeatedColumn("wijknaam")
	Gemeentecode = NewReapeatedColumn("gemeentecode")
	Gemeentenaam = NewReapeatedColumn("gemeentenaam")
	Provincienaam = NewReapeatedColumn("provincienaam")
	Provinciecode = NewReapeatedColumn("provinciecode")
	Gebruiksdoelen = NewReapeatedColumn("gebruiksdoelen")

}

func CreateMapstore() ModelMaps {
	return ModelMaps{
		Pid,
		Postcode,
		Woonplaatsnaam,
		Huisletter,
		Huisnummertoevoeging,
		Netbeheerder,
		Energieklasse,
		WoningType,
		Sbicode,
		Buurtcode,
		Buurtnaam,
		Wijkcode,
		Wijknaam,
		Gemeentecode,
		Gemeentenaam,
		Provincienaam,
		Provinciecode,
	}
}

func LoadMapstore(m ModelMaps) {

	Pid = m.Pid
	Postcode = m.Postcode
	Woonplaatsnaam = m.Woonplaatsnaam
	Huisletter = m.Huisletter
	Huisnummertoevoeging = m.Huisnummertoevoeging
	Netbeheerder = m.Netbeheerder
	Energieklasse = m.Energieklasse
	WoningType = m.WoningType
	Sbicode = m.Sbicode
	Buurtcode = m.Buurtcode
	Buurtnaam = m.Buurtnaam
	Wijkcode = m.Wijkcode
	Wijknaam = m.Wijknaam
	Gemeentecode = m.Gemeentecode
	Gemeentenaam = m.Gemeentenaam
	Provincienaam = m.Provincienaam
	Provinciecode = m.Provinciecode

	RegisteredColumns[Pid.Name] = Pid
	RegisteredColumns[Postcode.Name] = Postcode
	RegisteredColumns[Woonplaatsnaam.Name] = Woonplaatsnaam
	RegisteredColumns[Huisletter.Name] = Huisletter
	RegisteredColumns[Huisnummertoevoeging.Name] = Huisnummertoevoeging
	RegisteredColumns[Netbeheerder.Name] = Netbeheerder
	RegisteredColumns[Energieklasse.Name] = Energieklasse
	RegisteredColumns[WoningType.Name] = WoningType
	RegisteredColumns[Sbicode.Name] = Sbicode
	RegisteredColumns[Buurtcode.Name] = Buurtcode
	RegisteredColumns[Buurtnaam.Name] = Buurtnaam
	RegisteredColumns[Wijkcode.Name] = Wijkcode
	RegisteredColumns[Wijknaam.Name] = Wijknaam
	RegisteredColumns[Gemeentecode.Name] = Gemeentecode
	RegisteredColumns[Gemeentenaam.Name] = Gemeentenaam
	RegisteredColumns[Provincienaam.Name] = Provincienaam
	RegisteredColumns[Provinciecode.Name] = Provinciecode

}
